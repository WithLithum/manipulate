﻿// (C) 2022 WithLithum & contributors.
// Licensed under GNU General Public License, either version 3, or
// (at your opinion) any later version.

namespace WithLithum.Manipulate.CodeProcessing.Util;

public interface IProcessable
{
    List<string> Lines { get; }
    Dictionary<string, string> Directives { get; }

    void Process(IProcessor processor);
}