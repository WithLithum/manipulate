﻿// (C) 2022 WithLithum & contributors.
// Licensed under GNU General Public License, either version 3, or
// (at your opinion) any later version.

namespace WithLithum.Manipulate.CodeProcessing.Util;

public class LanguageSyntaxFault
{
    public LanguageSyntaxFault(string msg, int id)
    {
        SyntaxMessage = msg;
        Id = id;
    }

    internal string SyntaxMessage { get; set; }
    internal int Id { get; set; }
}