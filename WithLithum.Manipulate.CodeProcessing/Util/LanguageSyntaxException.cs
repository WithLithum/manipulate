﻿// (C) 2022 WithLithum & contributors.
// Licensed under GNU General Public License, either version 3, or
// (at your opinion) any later version.

namespace WithLithum.Manipulate.CodeProcessing.Util;

using System;

[Serializable]
public class LanguageSyntaxException : Exception
#pragma warning restore S3925 // "ISerializable" should be implemented correctly
{
    public int Line { get; set; }
    public int FaultIndex { get; set; }

    public LanguageSyntaxException()
    {
    }

    public LanguageSyntaxException(string? message) : base(message)
    {
    }

    public LanguageSyntaxException(LanguageSyntaxFault fault, int line) : base($"{fault.SyntaxMessage}")
    {
        Line = line;
        FaultIndex = fault.Id;
    }

    public LanguageSyntaxException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}