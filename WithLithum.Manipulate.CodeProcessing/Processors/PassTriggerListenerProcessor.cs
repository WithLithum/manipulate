﻿// (C) 2022 WithLithum & contributors.
// Licensed under GNU General Public License, either version 3, or
// (at your opinion) any later version.

namespace WithLithum.Manipulate.CodeProcessing.Processors;

using System.Collections.Generic;
using System.Text;
using WithLithum.Manipulate.CodeProcessing.Util;

public sealed class PassTriggerListenerProcessor : IProcessor
{
    public void Run(IProcessable processable)
    {
        var dict = new Dictionary<int, string>();

        for (int i = 0; i < processable.Lines.Count; i++)
        {
            if (processable.Lines[i].StartsWith("pass_trigger")) dict.Add(i, processable.Lines[i]);
        }

        foreach (var ring in dict)
        {
            var splitted = ring.Value.Split(' ');
            if (splitted.Length != 3)
            {
                throw new LanguageSyntaxException(SyntaxErrors.InvalidTriggerReference, ring.Key);
            }

            var sb = new StringBuilder();
            sb.Append("execute as @a[scores={");
            sb.Append(splitted[1]);
            sb.Append("=1..}] run function ");
            sb.Append(splitted[2]);

            processable.Lines[ring.Key] = sb.ToString();
        }
    }
}