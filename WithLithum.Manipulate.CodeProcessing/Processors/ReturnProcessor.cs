﻿// (C) 2022 WithLithum & contributors.
// Licensed under GNU General Public License, either version 3, or
// (at your opinion) any later version.

namespace WithLithum.Manipulate.CodeProcessing.Processors;

using System.Collections.Generic;
using System.Text;
using WithLithum.Manipulate.CodeProcessing.Util;

public sealed class ReturnProcessor : IProcessor
{
    public void Run(IProcessable processable)
    {
        var dict = new Dictionary<int, string>();

        for (int i = 0; i < processable.Lines.Count; i++)
        {
            if (processable.Lines[i] == "return") dict.Add(i, processable.Lines[i]);
        }

        foreach (var ring in dict)
        {
            var sb = new StringBuilder();
            sb.Append("scoreboard players set @s ")
                .Append(processable.Directives["this"])
                .Append(" 0");

            processable.Lines[ring.Key] = sb.ToString();
        }
    }
}