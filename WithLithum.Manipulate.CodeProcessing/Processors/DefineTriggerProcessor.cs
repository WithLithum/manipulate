﻿// (C) 2022 WithLithum & contributors.
// Licensed under GNU General Public License, either version 3, or
// (at your opinion) any later version.

namespace WithLithum.Manipulate.CodeProcessing.Processors;

using System.Text;
using WithLithum.Manipulate.CodeProcessing.Util;

public sealed class DefineTriggerProcessor : IProcessor
{
    private const string ScoreboardDefine = "scoreboard objectives add ";

    public void Run(IProcessable processable)
    {
        var dict = new Dictionary<int, string>();

        for (int i = 0; i < processable.Lines.Count; i++)
        {
            if (processable.Lines[i].StartsWith("def_trigger")) dict.Add(i, processable.Lines[i]);
        }

        foreach (var ring in dict)
        {
            var splitted = ring.Value.Split(' ');
            if (splitted.Length != 2)
            {
                throw new LanguageSyntaxException(SyntaxErrors.InvalidTriggerDefinition, ring.Key);
            }

            var sb = new StringBuilder();
            sb.Append(ScoreboardDefine);
            sb.Append(splitted[1]);
            sb.Append(" trigger");

            processable.Lines[ring.Key] = sb.ToString();
        }
    }
}