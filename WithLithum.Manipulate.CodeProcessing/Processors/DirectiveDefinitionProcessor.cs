﻿// (C) 2022 WithLithum & contributors.
// Licensed under GNU General Public License, either version 3, or
// (at your opinion) any later version.

namespace WithLithum.Manipulate.CodeProcessing.Processors;

using System.Linq;
using System.Text.RegularExpressions;
using WithLithum.Manipulate.CodeProcessing.Util;

public sealed class DirectiveDefinitionProcessor : IProcessor
{
    public void Run(IProcessable processable)
    {
        var registerLines = (from line in processable.Lines
                             where line.StartsWith("define")
                             select line).ToList();

        if (registerLines.Count == 0)
        {
            // No lines to process, return
            return;
        }

        foreach (var registerLine in registerLines)
        {
            var split = registerLine.Split(' ');
            if (split.Length != 3)
            {
                throw new LanguageSyntaxException(SyntaxErrors.InvalidDirectiveDefinition, processable.Lines.IndexOf(registerLine));
            }

            if (string.IsNullOrWhiteSpace(split[1]))
            {
                throw new LanguageSyntaxException(SyntaxErrors.InvalidDirectiveName, processable.Lines.IndexOf(registerLine));
            }

            var rgx = new Regex("[a-zA-Z1-9_]");
            if (!rgx.IsMatch(split[1]))
            {
                throw new LanguageSyntaxException(SyntaxErrors.InvalidDirectiveName, processable.Lines.IndexOf(registerLine));
            }

            if (processable.Directives.ContainsKey(split[1]))
            {
                throw new LanguageSyntaxException(SyntaxErrors.AlreadyExistingDirective, processable.Lines.IndexOf(registerLine));
            }

            processable.Directives.Add(split[1], split[2]);
            processable.Lines.Remove(registerLine);
        }
    }
}