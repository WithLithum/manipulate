﻿// (C) 2022 WithLithum & contributors.
// Licensed under GNU General Public License, either version 3, or
// (at your opinion) any later version.

namespace WithLithum.Manipulate.CodeProcessing.Processors;

using WithLithum.Manipulate.CodeProcessing.Util;

public sealed class DirectiveReferenceProcessor : IProcessor
{
    public void Run(IProcessable processable)
    {
        for (int i = 0; i < processable.Lines.Count; i++)
        {
            string? line = processable.Lines[i];
            foreach (var dir in processable.Directives)
            {
                if (line.Contains(dir.Key))
                {
                    processable.Lines[i] = line.Replace(dir.Key, dir.Value);
                }
            }
        }
    }
}