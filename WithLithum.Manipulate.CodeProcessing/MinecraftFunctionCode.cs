﻿// (C) 2022 WithLithum & contributors.
// Licensed under GNU General Public License, either version 3, or
// (at your opinion) any later version.

namespace WithLithum.Manipulate.CodeProcessing;

using System.Reflection;
using WithLithum.Manipulate.CodeProcessing.Util;

/// <summary>
/// Represents a <c>mcfunction</c> file.
/// </summary>
public class MinecraftFunctionCode : IProcessable
{
    /// <summary>
    /// Initializes a new instance of the <see cref="MinecraftFunctionCode"/> class.
    /// </summary>
    /// <param name="lines">The lines to process.</param>
    public MinecraftFunctionCode(string[] lines, string name)
    {
        Lines = lines.ToList();
        Directives.Add("this", name);
    }

    /// <summary>
    /// Gets the content of this instance.
    /// </summary>
    public List<string> Lines { get; }

    public Dictionary<string, string> Directives { get; } = new Dictionary<string, string>();

    public void Process(IProcessor processor)
    {
        processor.Run(this);
    }

    public void BuiltInProcessAll(StreamWriter writer)
    {
        var assem = Assembly.GetExecutingAssembly();
        var processors = (from type in assem.GetTypes()
                          where (!type.IsInterface && type.GetInterface("IProcessor") != null)
                          select type).ToArray();

        foreach (var processor in processors)
        {
            if (processor == null)
            {
                Console.WriteLine("Warning: found a null processor");
                continue;
            }

            var inst = Activator.CreateInstance(processor);

            if (inst == null)
            {
                Console.WriteLine("Warning: found an empty instance");
                continue;
            }

            if (inst is not IProcessor obj)
            {
                Console.WriteLine("Warning: found invalid instance");
                continue;
            }

            Process(obj);
        }

        foreach (var line in Lines)
        {
            writer.WriteLine(line);
        }
    }
}