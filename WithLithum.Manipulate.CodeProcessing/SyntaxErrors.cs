﻿// (C) 2022 WithLithum & contributors.
// Licensed under GNU General Public License, either version 3, or
// (at your opinion) any later version.

namespace WithLithum.Manipulate.CodeProcessing;

using WithLithum.Manipulate.CodeProcessing.Util;

internal static class SyntaxErrors
{
    internal static readonly LanguageSyntaxFault InvalidDirectiveDefinition =
        new(SyntaxErrorResources.InvalidDirectiveDefinition, 1);

    internal static readonly LanguageSyntaxFault InvalidDirectiveName =
        new(SyntaxErrorResources.InvalidDirectiveName, 2);

    internal static readonly LanguageSyntaxFault InvalidTriggerDefinition =
        new(SyntaxErrorResources.InvalidTriggerDefinition, 3);

    internal static readonly LanguageSyntaxFault InvalidTriggerReference =
        new(SyntaxErrorResources.InvalidTriggerReference, 4);

    internal static readonly LanguageSyntaxFault AlreadyExistingDirective =
        new(SyntaxErrorResources.AlreadyExistingDirective, 5);

    internal static readonly LanguageSyntaxFault InvalidInstructionSyntax =
        new(SyntaxErrorResources.InvalidInstructionSyntax, 6);
}