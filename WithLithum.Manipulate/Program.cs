﻿using WithLithum.Manipulate.CodeProcessing;
using WithLithum.Manipulate.CodeProcessing.Util;

if (args.Length != 1)
{
    Console.WriteLine("manpic: Invalid parameters");
    return 1;
}

if (!File.Exists(args[0]))
{
    Console.WriteLine("manpic: File does not exist");
    return 1;
}

var lines = File.ReadAllLines(args[0]);
if (lines == null || lines.Length == 0)
{
    Console.WriteLine("manpic: File content empty or failed to read properly");
    return 1;
}

var mfc = new MinecraftFunctionCode(lines, Path.GetFileNameWithoutExtension(args[0]));

using (var writer = new StreamWriter(Path.ChangeExtension(args[0], "mcfunction")))
{
    try
    {
        mfc.BuiltInProcessAll(writer);
    }
    catch (LanguageSyntaxException ex)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.WriteLine($"Line {ex.Line + 1} - MPI{ex.FaultIndex:d4}: {ex.Message}");
        Console.ResetColor();
        return 2;
    }
}

Console.WriteLine($"{Path.GetRelativePath(Environment.CurrentDirectory, args[0])} -> {Path.GetRelativePath(Environment.CurrentDirectory, Path.ChangeExtension(args[0], "mcfunction"))}");

return 0;