namespace WithLithum.Manipulate.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WithLithum.Manipulate.CodeProcessing;
using WithLithum.Manipulate.CodeProcessing.Processors;

[TestClass]
public class ProcessorTests
{
    /// <summary>
    /// Tests the <see cref="ReturnProcessor"/>.
    /// </summary>
    /// <remarks>
    /// This test generates an in-memory instance of <see cref="MinecraftFunctionCode"/> with only
    /// one line of <c>return</c>, and an instance of <see cref="ReturnProcessor"/> which performs
    /// the processing. After done, this test checks whether the output code is <c>scoreboard players set @s testFile 0</c>,
    /// and if not, this test fails.
    /// </remarks>
    [TestMethod]
    public void ReturnProcessorTest()
    {
        var testfile = new MinecraftFunctionCode(new string[]{ "return" }, "testFile");
        var proc = new ReturnProcessor();

        testfile.Process(proc);

        Assert.IsTrue(testfile.Lines[0] == "scoreboard players set @s testFile 0");
    }

    [TestMethod]
    public void TriggerProcessorsTest()
    {
        var testFile = new MinecraftFunctionCode(new string[] { "def_trigger mstest",
            "pass_trigger mstest test:a/mstest"}, "testFile");

        testFile.Process(new DefineTriggerProcessor());
        testFile.Process(new PassTriggerListenerProcessor());

        Assert.IsTrue(testFile.Lines[0] == "scoreboard objectives add mstest trigger");
        Assert.IsTrue(testFile.Lines[1] == "execute as @a[scores={mstest=1..}] run function test:a/mstest");
    }

    [TestMethod]
    public void ClearScoreTest()
    {
        var testFile = new MinecraftFunctionCode(new string[] { "clear_score @s testScore" }, "testFile");

        testFile.Process(new ClearScoreProcessor());
        Console.WriteLine("Created command: " + testFile.Lines[0]);
        Assert.IsTrue(testFile.Lines[0] == "scoreboard players set @s testScore 0");
    }

    [TestMethod]
    public void DefineTest()
    {
        var testFile = new MinecraftFunctionCode(new string[] { "define testVal correct", "say testVal" }, "testFile");

        testFile.Process(new DirectiveDefinitionProcessor());
        testFile.Process(new DirectiveReferenceProcessor());

        Console.WriteLine("Created command: " + testFile.Lines[0]);
        Assert.IsTrue(testFile.Lines[0] == "say correct");
    }
}